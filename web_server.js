// Make it possible to read a .json file as a json object
require.extensions[".json"] = function (m) {
	m.exports = JSON.parse(require("fs").readFileSync(m.filename));
};

// Node modules
var fs = require("fs");
var server = require("http");
// Local JS
var utils = require("./utils.js");
var colours = require("./colours.js");
var fileType = require("./fileTypes.js");
// Local JSON
var options = require("./options.json");
var userErrors = require("./userErrors.json");

// Initialise options
var indexPage = options.pages.index;
var wwwFolder = __dirname + options.www_folder;
var directoryPage = options.directory.template;
var directoryTruncateLevel = options.directory.truncate_level || 5;
// 404 page is only known after "getFile" has run, so we need the raw data
// 405 page gets passed as a filename to "getFile"
var _404Page = fs.readFileSync(wwwFolder + options.pages._404);
var _405Page =  options.pages._405;

// Magic padding value!
var DIR_PADDING = 51;

// Allowed requests
var ALLOWED_REQUESTS = ["GET", "HEAD"];

// Very basic way of disabling logging
if(!options.logging.enabled){
	console.log = function(){};
}

colours.enabled = options.logging.colour;

function startServer(){
	server = server.createServer(handleRequest);
	server.on("error", errorHandler);
	server.listen(options.port);
	console.log("Server started on port:", colours.style.BRIGHT(options.port));
}

function handleRequest(request, response) {
		// Decode things like %20 to a space
		request.url = decodeURI(request.url);
		// If the root URL is requested, return the index page
		if(indexPage !== "" && request.url.trim() === "/"){
			request.url = indexPage;
		}

		// Process the request
		switch(request.method){
			case "GET": // Falls through
			case "HEAD":
				fs.lstat(wwwFolder + request.url, function(err, stats){
					// Is the requested file a directory?
					if(options.directory.enabled && stats && stats.isDirectory()){
						// If it is, and it doesn't end with a "/"
						if(request.url.substr(-1) !== "/"){
							// Then redirect the browser to the same place, but with the "/" at the end.
							var responseCode = 302;
							response.writeHead(responseCode, {
								"Content-Type": 	fileType.getContentType("html"),
								"location": 		request.url + "/"
							});
							response.end();
							log(request.method, request.url, responseCode);
						}else{
							// We should send the directory page to the browser
							getDirectory(request, send(response));
						}
					}else{
						// We should send the file to the browser
						getFile(request, send(response));
					}
				});
				break;
			default:
				// It's not an allowed request, so we send the 405 page
				// Overwrite the request with our custom one, as we know what we're doing
				getFile({
					method: request.method,
					url: _405Page
				}, send(response, 405));
		}
	}

	function errorHandler(err){
		var errorMsg;
	// If there are some custom user errors, we should use those instead of the generic node error
	if(options.user_error){
		errorMsg = userErrors[err.code];
	}

	// Do this last in case userErrors returns nothing
	if(!errorMsg){
		errorMsg = err;
	}
	error(errorMsg);
}

function send(response, overrideResponseCode){
	return function(request, responseCode, data, contentType){
		// Useful for the 405 error, as we get a 200 because we've found the 405 html page
		responseCode = overrideResponseCode || responseCode;
		response.writeHead(responseCode, {
			"Content-Type": 	contentType,
			"Allow": 			"GET, HEAD"
		});
		if(request.method !== "HEAD"){
			response.write(data);
		}
		response.end();
		log(request.method, request.url, responseCode);
	};
}

function getStatsHTML(dirName){
	var items = fs.readdirSync(wwwFolder + dirName);
	var htmlFolders = "";
	var htmlFiles = "";

	for(var i in items){
		var html = "";
		var size = "";
		var path = dirName + items[i];

		// Get the information about the file/folder
		var stats = fs.statSync(wwwFolder + path);

		if(stats.isDirectory()){
			// If it's a folder, add "/" to the end of the path
			// so we don't have to redirect the browser every time
			// a folder is clicked on
			path += "/";
		} else {
			// Convert to human friendly values (KB, MB etc) if it's a file
			size = utils.toHumanFriendlySize(stats.size);
		}
		var lastModified = stats.mtime.toUTCString();

		// Link to the file/folder
		html += "<tr>";
			html += "<td>" + `<a href="${path}">${truncatePath(path)}</a>` + "</td>";
			html += "<td>" + lastModified + "</td>";
			html += "<td>" + size + "</td>";
		html += "</tr>";
		// By default, fs.readdir will return everything in alphabetical order
		// we want folders to be shown first, and files second
		if(stats.isDirectory()){
			htmlFolders += html;
		}else{
			htmlFiles += html;
		}
	}


	// Folders then files, both sorted alphabetically
	return {
		folders: htmlFolders,
		files: htmlFiles
	};
}

function truncatePath(path, split="/") {
	var ret = "";
	var localTruncateLevel = directoryTruncateLevel;

	path = path.substr(1); // The first char is always the split char
	var splitPath = path.split(split);
	if (splitPath.at(-1) == "") {
		// For directories, the last element is an empty string
		// add 1 to the truncate level to account for it
		localTruncateLevel++;
	}

	if (splitPath.length <= directoryTruncateLevel) {
		// The path isn't too long yet
		ret = split + path;
	} else {
		// Too long, get the last X many and join those
		var lastX = splitPath.slice(-directoryTruncateLevel);
		// Add a parent directory placeholder at the beginning
		ret = ".." + split + lastX.join(split);
	}

	return ret;
}

function getDirectory(request, callback){
	// Load the directory page into *data*, as a utf8 file
	fs.readFile(wwwFolder + directoryPage, 'utf8', function(err, data){
		var url = request.url
		var items = getStatsHTML(url);
		// Replace the {{title}} with the current directory
		data = data.replaceAll("{{title}}", url);
		// data = data.replaceAll("{{items}}", getStatsHTML(request.url));
		data = data.replaceAll("{{folders}}", items.folders);
		data = data.replaceAll("{{files}}", items.files);
		callback(request, 200, data, fileType.getContentType("html"));
	});
}

function getFile(request, callback){
	var fileName = request.url;
	var fileExtension = /.*\.(.+)/.exec(fileName);

	// TODO: the file might not have an extension, check if it exists first
	if(fileExtension === null){
		fileName += ".html";
		fileExtension = "html";
	}else{
		fileExtension = fileExtension[1];
	}

	fs.readFile(wwwFolder + fileName, function(err, data){
		var contentType = fileType.getContentType(fileExtension);
		if(err){
			callback(request, 404, _404Page, fileType.getContentType("html"));
		}else{
			callback(request, 200, data, contentType);
		}
	});
}

// TODO: Add a date/time
function log(method, url, result){
	// The colours class will return the input string if options.logging.colour is false
	method = colours.bg.CYAN(method);
	url = colours.bg.WHITE(colours.fg.BLACK(url));
	switch(result){
		case 405: // Falls through
		case 404:
			result = colours.bg.RED(result);
			break;
		case 302:
			result = colours.style.DIM(colours.bg.YELLOW(result));
			break;
		case 200:
			result = colours.bg.GREEN(result);
			break;
	}

	// Make sure we use the separator from the options file
	var _ = options.logging.seperator || "";
	console.log(method + _ + url + _ + result);
}

function error(errMsg){
	// The colours class will return the input string if options.logging.colour is false
	errMsg = colours.fg.WHITE(errMsg);
	errMsg = colours.bg.RED(errMsg);
	console.log(errMsg);
}

startServer();