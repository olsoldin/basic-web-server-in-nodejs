var colours = {};

colours.enabled = true;

colours.apply = function(col){
	return function(str){
		if(colours.enabled){
			return col + str + colours.style._RESET_ALL;
		}else{
			return str;
		}
	};
};

colours.fg = {
	BLACK: 		colours.apply('\033[30m'),
	RED: 		colours.apply('\033[31m'),
	GREEN: 		colours.apply('\033[32m'),
	YELLOW: 	colours.apply('\033[33m'),
	BLUE: 		colours.apply('\033[34m'),
	MAGENTA: 	colours.apply('\033[35m'),
	CYAN: 		colours.apply('\033[36m'),
	WHITE: 		colours.apply('\033[37m'),
	RESET: 		colours.apply('\033[39m')
};

colours.bg = {
	BLACK: 		colours.apply('\033[40m'),
	RED: 		colours.apply('\033[41m'),
	GREEN: 		colours.apply('\033[42m'),
	YELLOW: 	colours.apply('\033[43m'),
	BLUE: 		colours.apply('\033[44m'),
	MAGENTA: 	colours.apply('\033[45m'),
	CYAN: 		colours.apply('\033[46m'),
	WHITE: 		colours.apply('\033[47m'),
	RESET: 		colours.apply('\033[49m')
};

colours.style = {
	BRIGHT: 	colours.apply('\033[1m'),
	DIM: 		colours.apply('\033[2m'),
	NORMAL: 	colours.apply('\033[22m'),
	_RESET_ALL: '\033[0m'
};

module.exports = colours;