# README #

### What is this repository for? ###

This is a simple project for hosting HTML files on a network without any complicated server setup.

### How do I start it? ###

You need to have NodeJS installed on your system.
If you don't have Node, you can download it from [here](https://nodejs.org/en/).

You can start the server using `node web_server.js`

### What are the options? ###

There is a file named `options.json`, this is what all the options mean:

`port` - the port the server will listen on, make sure it is set to a free port, or you will get an `Error: listen EACCES` message.

`www_folder` - the root folder where the server will be sending files from. Note that this is a relative path, not an absolute one.

`logging.enabled` - this turns logging on or off, if it is on, every request to the server is printed to the console.

`logging.colour` - this colours the output in the console. It makes it easier to see at a glance what is happening.

`logging.seperator` - the separator of the log output, in case you pipe the log to a file and want to use it with some other software.

`directory.enabled` - this turns directory traversal on or off.

`directory.template` - the template file for the directory traversal page.

`directory.truncate_level` - how many directory levels will be shown before it they get truncated to /../

`pages.index` - the root page of the server. When this is left blank, the root page will be a directory browser (if directory traversal is enabled).

`pages._404` - the 404 page that gets returned when the requested resource can't be found.

`pages._405` - the 405 page that gets returned when an invalid http method is given (not HEAD or GET).

### Notes ###

In `fileTypes.json` there is a list of hundreds of file-types, each with it's own MIME type assigned to it.
It should enable browsers to show the data in the best way it can, but there is a chance that some of them
are wrong causing the browser to show the data incorrectly.