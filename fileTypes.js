var types = {};

types.MIME = require("./fileTypes.json");

types.getContentType = function(fileType){
	// RFC 2046 4.5.1:
	// The "octet-stream" subtype is used to indicate that a body contains arbitrary binary data.
	return types.MIME[fileType] || "application/octet-stream";
};

module.exports = types;