var utils = {};

utils.toHumanFriendlySize = function(size){
	// If the size is 0, replace it with "-"
	if(size == 0){
		return "-";
	}
	// EXTREMELY scalable
	var siPrefixList = ['kB', 'MB', 'GB', 'TB', 'PB', 'EB'];
	var label = 'B';
	// Try getting the size between 0 and 1024 for an easier to read number
	// Keep reducing the size to the next lower prefix, while also moving up the prefix list
	for(var i = 0; size > 1024 && i < siPrefixList.length; i++){
		size /= 1024;
		label = siPrefixList[i];
	}
	// Return the size to 2 decimal places, with the label at the end
	return size.toFixed(2) + " " + label;
}

String.prototype.replaceAll = function(search, replacememt) {
	// Escape all regex
	search.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
	return this.replace(new RegExp(search, 'g'), replacememt);
};

module.exports = utils